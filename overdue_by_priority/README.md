Script which displays a table of issue counts, grouping issues by priority (rows) and whether they were resolved before, after or on the due date (columns).


|                            | Total | Unfinished | Finished On Due | Finished Before Due | Finished After Due | Finished, no due date |
|----------------------------|-------|------------|-----------------|---------------------|--------------------|-----------------------|
| Blocker, without due date  | 6     | 1          | 0               | 0                   | 0                  | 5                     |
| Critical, without due date | 4     | 1          | 0               | 0                   | 0                  | 3                     |
| Major, without due date    | 156   | 75         | 0               | 0                   | 0                  | 81                    |
| Minor, without due date    | 19    | 12         | 0               | 0                   | 0                  | 7                     |
| Major, with due date       | 2     | 2          | 0               | 0                   | 0                  | 0                     |

To run:

bundle install
cp vars.rb.example vars.rb
vim vars.rb			 # Set your details in vars.rb
vim jira_overdue_by_priority_report.rb		# Edit the JQL expression
bundle exec ./jira_overdue_by_priority_report.rb
# Or pass in JQL as an argument:
bundle exec ./jira_overdue_by_priority_report.rb "project=UX and issuetype=Bug and updated>='2015-10-16' AND updated<='2015-10-27'"

For details, see https://www.redradishtech.com/pages/viewpage.action?pageId=1081399
