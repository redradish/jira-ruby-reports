#!/usr/bin/env ruby

require 'jira'
require 'trollop'
#require 'pry'


require_relative './vars'

opts = Trollop::options do
    banner <<-EOS
Usage: jira_overdue_by_priority_report.rb [options] [username]
EOS

    opt :project, "Project Key", :short => 'p', :default => 'UX'
    opt :issuetype, "Issue type", :short => 't', :type=>:string, :default => 'Bug'
    opt :to, "From Date", :type=>:date, :default => Date.today
    opt :from, "From Date", :type=>:date, :default => Date.today - 30
end


$options = {
  :site => JIRA_URL,
  :context_path => '',
  :username => JIRA_USERNAME,
  :password => JIRA_PASSWORD,
  :auth_type => :basic
}

SEVERITY="customfield_12900"

# Return 'Severity' custom field, if available, 'Priority' otherwise
def priority(i)
	if i.respond_to?(SEVERITY) then
		i.send(SEVERITY) ? i.send(SEVERITY)["value"] : "Unknown Severity"
	else
		i.priority ? i.priority.name : "Unknown Priority"
	end
end

$client = JIRA::Client.new($options)
#query = %Q{project='#{opts[:project]}' AND issuetype = '#{opts[:issuetype]}' AND created >= '#{opts[:from].strftime('%Y-%m-%d')}' AND created <= '#{opts[:to].strftime('%Y-%m-%d')}'}
query = %Q{project='#{opts[:project]}' AND issuetype = '#{opts[:issuetype]}'}
issues = $client.Issue.jql(query, max_results:1000)
raise 'Hit 1000 issue limit' if issues.size==1000


data = issues.group_by { |i| priority(i) + ", " + (i.duedate ? "with" : "without") + " due date" }.
	inject({}) { |h, (priority, issues)|
			h[priority] = issues.group_by { |i|
				resdate = i.resolutiondate && DateTime.strptime(i.resolutiondate, '%Y-%m-%dT%H:%M:%S.%L%z').to_date
				duedate = i.duedate && Date.strptime(i.duedate, "%Y-%m-%d")
				if !resdate then "Unfinished"	# Unresolved
					elsif resdate<opts[:from] or resdate>opts[:to] then "Finished outside daterange"
					elsif !duedate then "Finished, no due date"
					elsif resdate == duedate then "Finished On Due"
					elsif resdate < duedate then "Finished Before Due"
					elsif resdate > duedate then "Finished After Due"
					else raise "Impossible"
				end
			}
			h[priority]["Total"] = issues
			h
	 	}
#binding.pry

#cols = data.collect { |(k,v)| v.keys  }.flatten.uniq  # Identify unique columns.
cols = ["Total", "Unfinished", "Finished Before Due", "Finished On Due", "Finished After Due", "Finished, no due date", "Finished outside daterange"]
result = [[nil] + cols]	# First row is a list of columns, starting with a nil
# Add rows, consisting of an array beginning with 'rowname', followed by the number of issues, or zero
result += data.collect { |(priority, issues_by_finishedstatus)|
	[priority] + cols.collect { |col|
		 issues_by_finishedstatus[col] ? issues_by_finishedstatus[col].size : 0 }
	}
require 'pp'
#puts "-"*50
#pp result
puts "-"*50
puts "| " + result.collect { |r| r.collect.with_index { |c,i| 
		colwidth = (i==0 ? 34 : result[0][i].size)
		"%-#{colwidth}s" % c }.join(" | ") 
	}.join(" |\n| ") + " |"
puts "-"*50
puts "<table border=1>\n" + 
	result.map.with_index { |r, i| "<tr>" +
		 r.map.with_index { |c, j|
		 	el = (i==0 || j==0 ? "th" : "td")
			"<#{el}>" + (c ? c.to_s : "\t") + "</#{el}>" }.join +
			 "</tr>"
			 }.join("\n") +
      "</table>"
