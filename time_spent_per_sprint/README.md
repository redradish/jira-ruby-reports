Script which displays the total worklog times logged per user, in a given sprint. E.g.


jsmith: 32h 16m
afernando: 29h 30m
jalison: 37h 12m
oporter: 44h 40m
dnewlands: 32h 30m
ballen: 0h 1m
bob: 12h 31m

To run:

bundle install
cp vars.rb.example vars.rb
vim vars.rb			 # Set your details in vars.rb
vim time_per_user_per_sprint.rb		# Set the sprint ID/Name in the JQL expression
bundle exec ./time_per_user_per_sprint.rb
# Or pass in JQL as an argument:
bundle exec ./time_per_user_per_sprint.rb "sprint=Quintara"


Similarly, a script that prints total time spent and Story Points resolved during a sprint:

bundle exec ./sprint_total_time.rb
Total time spent:	224.000000
Total Story Points resolved:	61


For more details, see https://www.redradishtech.com/display/KB/JIRA+Report%3A+time+worked+per+user%2C+per+sprint
