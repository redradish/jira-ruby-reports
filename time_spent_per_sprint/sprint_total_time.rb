#!/usr/bin/env ruby

require 'jira'
#require 'pry'
require 'parallel'

require_relative './vars'
$options = {
  :site => JIRA_URL,
  :context_path => '',
  :username => JIRA_USERNAME,
  :password => JIRA_PASSWORD,
  :auth_type => :basic
}

$client = JIRA::Client.new($options)
# Fetch the issues in 10 parallel processes
issues = Parallel.map($client.Issue.jql("sprint=251", max_results:1000), :in_processes=>10) { |i| i.fetch; i }
puts "Total time spent:\t%f\n" % (issues.collect { |i| i.timespent or 0 }.inject(:+) / 60 / 60)
puts "Total Story Points resolved:\t%d\n" % issues.collect { |i| i.customfield_10105 or 0 }.inject(:+)
