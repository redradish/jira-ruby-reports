#!/usr/bin/env ruby

require 'jira'
#require 'pry'
require 'parallel'

require_relative './vars'
$options = {
  :site => JIRA_URL,
  :context_path => '',
  :username => JIRA_USERNAME,
  :password => JIRA_PASSWORD,
  :auth_type => :basic
}

query=ARGV[0]
if !query then
	query="sprint=251"
	$stderr.puts "No JQL argument passed; using default: #{query}"
end

$client = JIRA::Client.new($options)
# Fetch the issues in 10 parallel processes
issues = Parallel.map($client.Issue.jql("sprint=251", max_results:1000), :in_processes=>10) { |i| i.fetch; i }
  .each_with_object({}) { |i,hash|
        i.worklogs.each { |w|
            hash[w.author.name] ||= 0
            hash[w.author.name] += w.timeSpentSeconds
        } }
    .each { |user, secs|
        puts "#{user}: #{secs / 60 / 60}h #{secs / 60 % 60}m"
    }
